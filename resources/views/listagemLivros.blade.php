<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
    <title>Livros</title>
    <style>
    table{
        font-family: 'Ubuntu';
    }
    body{
        font-family: 'Ubuntu';
        background-color: whitesmoke;
    }
    #col_main{
        font-weight:bold;
    }
    </style>
</head>
<body>
    <center><h1>Livros</h1>
    <table border="2px">
    <div id="col_main">
    <tr>
    <td>ID do livro</td>
    <td>Nome do livro</td>
    <td>ID do autor</td>
    <td>ID da editora</td>
    </tr>
    </div>
    @foreach($livros as $livros)
    <tr>
    <td>{{$livros->id}} </td>
    <td>{{$livros->nome}} </td>
    <td>{{$livros->id_autor}} </td>
    <td>{{$livros->id_editora}} </td>
    </tr>
    @endforeach
    </table> <!--Tabela-->
    <br><br>
    <a href="/"><button>Clique aqui para retornar para a página inicial</button></a>
</center>


</body>
</html>
